﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Nano.Console.Utils
{
    public static class Colors
    {
        public static ConsoleColor Heading { get; set; } = ConsoleColor.White;
        public static ConsoleColor Emphasized { get; set; } = ConsoleColor.Cyan;
        public static ConsoleColor UserInput { get; set; } = ConsoleColor.DarkCyan;
        public static ConsoleColor Ident { get; set; } = ConsoleColor.DarkCyan;
        public static ConsoleColor Normal { get; set; } = ConsoleColor.White;
        public static ConsoleColor Success { get; set; } = ConsoleColor.Green;
        public static ConsoleColor Error { get; set; } = ConsoleColor.Red;

        public static ConsoleColor ErrorDetail { get; set; } = ConsoleColor.DarkRed;
        public static ConsoleColor Transmit { get; set; } = ConsoleColor.DarkGreen;
        public static ConsoleColor Receive { get; set; } = ConsoleColor.DarkRed;
        public static ConsoleColor Message { get; set; } = ConsoleColor.Gray;

        public static ConsoleColor Action { get; set; } = ConsoleColor.DarkMagenta;
        public static ConsoleColor Debug { get; set; } = ConsoleColor.Blue;
        public static ConsoleColor Warning { get; set; } = ConsoleColor.Yellow;
    }
}