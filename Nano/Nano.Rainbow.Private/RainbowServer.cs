﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Nano.WebSockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Nano.Rainbow.Private
{
    public delegate Task<byte[]> RestRequestHandler(Uri uri, Header header, byte[] body);

    public abstract class RainbowServer
    {
        public readonly Uri Host;
        public readonly IReporter Reporter;

        private readonly ConcurrentDictionary<string, RestRequestHandler> handlers = new ConcurrentDictionary<string, RestRequestHandler>();

        public bool CompressStaticContent { get; }

        public string HealthMessage { get; set; } = "GOOD";

        public HttpStatusCode HealthResult { get; set; } = HttpStatusCode.OK;

        public Dictionary<string, string> MimeTypes { get; } = new Dictionary<string, string>();
        //private bool shouldExit;

        public DateTime Started { get; set; }

        public Dictionary<string, byte[]> StaticContent { get; } = new Dictionary<string, byte[]>();

        public string StaticContentPath { get; }

        protected RainbowServer(
            Uri host,
            string staticContentPath,
            bool compressStaticContent,
            IReporter reporter)
        {
            Host = host;
            Started = DateTime.Now;
            StaticContentPath = staticContentPath;
            CompressStaticContent = compressStaticContent;

            Reporter = reporter;

            AddHandler("/health", HandleHealth);

            AddMimeTypes();

            ScanStaticContentPath();
        }

        public bool AcceptsGzip(Header header)
        {
            return header.TryGetValue("Accept-Encoding", out string encoding) && encoding.Contains("gzip");
        }

        public static byte[] CompressDeflate(byte[] raw)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (DeflateStream gzip = new DeflateStream(memory, CompressionMode.Compress, true))
                {
                    gzip.Write(raw, 0, raw.Length);
                }

                return memory.ToArray();
            }
        }

        public static byte[] CompressGzip(byte[] raw)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (GZipStream gzip = new GZipStream(memory, CompressionMode.Compress, true))
                {
                    gzip.Write(raw, 0, raw.Length);
                }

                return memory.ToArray();
            }
        }

        public byte[] GetJsonResponse(Header header, bool compress, string body)
        {
            return GetMessageBytes(header, compress, "application/json", body);
        }

        public byte[] GetMessageBytes(
            Header header,
            bool compress,
            string contentType,
            string body)
        {
            return GetMessageBytes(header, compress, contentType, Encoding.UTF8.GetBytes(body));
        }

        public byte[] GetMessageBytes(
            Header header,
            bool compress,
            string contentType,
            byte[] body)
        {
            if (compress)
            {
                header.Add("Content-Encoding", "gzip");
                body = CompressGzip(body);
            }

            header.Add("Content-Type", contentType);
            header.Add("Content-Length", body.Length.ToString());

            using (MemoryStream buffer = new MemoryStream())
            {
                byte[] headerBytes = header.ToBytes();

                buffer.Write(headerBytes, 0, headerBytes.Length);
                buffer.Write(body, 0, body.Length);

                return buffer.ToArray();
            }
        }

        public void PrintObject<T>(T value)
        {
            if (Reporter.ReportVerbosity > ReportVerbosity.Normal)
            {
                return;
            }

            Reporter.PrintHeading(
                JToken.Parse(JsonConvert.SerializeObject(value))
                    .ToString(Formatting.Indented)
            );
        }
//
//        public virtual void Close()
//        {
//            shouldExit = true;
//        }

        public virtual async Task Start(CancellationToken cancel)
        {
            await Task.Yield();

            TcpListener tcpListener = TcpListener.Create(Host.Port);
            tcpListener.Start();

            Reporter.PrintEmphasized($"Started REST Service on: {Host}");

            while (cancel.IsCancellationRequested == false)
            {
                TcpClient tcpClient = await tcpListener.AcceptTcpClientAsync();

                Reporter.PrintDetail(Direction.Receive, Host.ToString(), "REST request");

                Task task = StartHandleConnection(tcpClient);

                // if already faulted, re-throw any error on the calling context
                if (task.IsFaulted)
                {
                    task.Wait(cancel);
                }
            }

            tcpListener.Stop();
        }

        protected bool AddHandler(string path, RestRequestHandler handler)
        {
            return handlers.TryAdd(path.ToLowerInvariant(), handler);
        }

        protected bool ContainsHander(string path)
        {
            return handlers.ContainsKey(path.ToLowerInvariant());
        }

        protected byte[] ErrorResponse(HttpStatusCode code, string message)
        {
            Reporter.PrintError(Direction.Transmit, Host.ToString(), message);

            return GetMessageBytes(new Header(code, message), false, "text/plain", message);
        }

        protected byte[] ExceptionResponse(HttpStatusCode code, string message, Exception ex)
        {
            Reporter.PrintException(ex, message);

            return GetMessageBytes(new Header(code, message), false, "text/plain", message);
        }

        protected bool RemoveHander(string path, out RestRequestHandler handler)
        {
            return handlers.TryRemove(path.ToLowerInvariant(), out handler);
        }

        private void AddMimeTypes()
        {
            MimeTypes.Add(".js", "application/javascript");
            MimeTypes.Add(".css", "text/css");
            MimeTypes.Add(".html", "text/html");
            MimeTypes.Add(".png", "image/png");
            MimeTypes.Add(".jpg", "image/jpeg");
            MimeTypes.Add(".eot", "application/vnd.ms-fontobject");
            MimeTypes.Add(".ttf", "application/octet-stream");
            MimeTypes.Add(".svg", "image/svg+xml");
            MimeTypes.Add(".woff", "application/font-woff");
            MimeTypes.Add(".woff2", "application/font-woff2");
            MimeTypes.Add(".txt", "text/plain");
        }

        private async Task EndHandleConnection(TcpClient tcpClient)
        {
            // continue asynchronously on another threads
            await Task.Yield();

            using (NetworkStream networkStream = tcpClient.GetStream())
            {
                byte[] buffer = new byte[4096];

                Reporter.PrintDetail(Direction.Receive, Host.ToString(), "Reading from client");

                int byteCount = await networkStream.ReadAsync(buffer, 0, buffer.Length);

                Header header = new Header(buffer, 0, byteCount);

                byte[] bodyBytes = new byte[0];

                if (header.TryGetValue("Content-Length", out string contentLengthString))
                {
                    if (int.TryParse(contentLengthString, out int contentLength) == false)
                    {
                        throw new Exception("Invalid Content-Length");
                    }

                    int position = header.HeaderLength;
                    int bodyPosition = 0;
                    int bytesRemaining = contentLength;
                    int bytesInCurrentBuffer = byteCount - header.HeaderLength;

                    bodyBytes = new byte[contentLength];

                    while (bytesRemaining > 0)
                    {
                        Array.ConstrainedCopy(buffer, position, bodyBytes, bodyPosition, bytesInCurrentBuffer);

                        bodyPosition += bytesInCurrentBuffer;
                        bytesRemaining -= bytesInCurrentBuffer;

                        if (bytesRemaining <= 0)
                        {
                            break;
                        }

                        bytesInCurrentBuffer = await networkStream.ReadAsync(buffer, 0, buffer.Length);
                        position = 0;
                    }
                }

                //Reporter.PrintDetail(Direction.Receive, Host.ToString(), $"Read {byteCount} bytes from client");

                Uri fullPath = new Uri(Host, header.Uri);

                if (fullPath.AbsolutePath.ToLowerInvariant() != "/health")
                {
                    Reporter.PrintNormal(Direction.Receive, fullPath.ToString(), $"({Helper.GetMemStringFromBytes(byteCount, true)})");
                }

                string urlAbsolutePath = fullPath.AbsolutePath.ToLowerInvariant();

                byte[] serverResponseBytes;

                if (StaticContent.TryGetValue(fullPath.AbsolutePath.ToLowerInvariant(), out byte[] staticContentFileBytes))
                {
                    string extension = new FileInfo(fullPath.AbsolutePath.ToLowerInvariant()).Extension.ToLowerInvariant();

                    if (MimeTypes.TryGetValue(extension, out string contentType) == false)
                    {
                        Reporter.PrintError(Direction.Receive, fullPath.ToString(), "Unknown content type");

                        serverResponseBytes = new Header(HttpStatusCode.NotFound).ToBytes();

                        await networkStream.WriteAsync(serverResponseBytes, 0, serverResponseBytes.Length);

                        return;
                    }

                    Header resultHeader = new Header(HttpStatusCode.OK);

                    if (CompressStaticContent)
                    {
                        resultHeader.Add("Content-Encoding", "gzip");
                    }

                    serverResponseBytes = GetMessageBytes(resultHeader, false, contentType, staticContentFileBytes);

                    Reporter.PrintNormal(Direction.Transmit, Host.ToString(), $"{fullPath.AbsolutePath.ToLowerInvariant()} {contentType} ({Helper.GetMemStringFromBytes(serverResponseBytes.Length, true)})");

                    await networkStream.WriteAsync(serverResponseBytes, 0, serverResponseBytes.Length);

                    return;
                }

                try
                {
                    serverResponseBytes = handlers.TryGetValue(fullPath.AbsolutePath.ToLowerInvariant(), out RestRequestHandler handler) == false ? new Header(HttpStatusCode.NotFound).ToBytes() : await handler(fullPath, header, bodyBytes);

                    Reporter.PrintDetail(Direction.Transmit, Host.ToString(), $"Write {serverResponseBytes.Length} bytes to client");

                    await networkStream.WriteAsync(serverResponseBytes, 0, serverResponseBytes.Length);
                }
                catch (Exception ex)
                {
                    serverResponseBytes = ExceptionResponse(HttpStatusCode.InternalServerError, "Server exception", ex);

                    await networkStream.WriteAsync(serverResponseBytes, 0, serverResponseBytes.Length);
                }
            }
        }

        private async Task<byte[]> HandleHealth(Uri uri, Header header, byte[] body)
        {
            return GetMessageBytes(new Header(HttpStatusCode.OK), AcceptsGzip(header), "text/plain", HealthMessage);
        }

        private void ScanStaticContentPath()
        {
            if (StaticContentPath == null)
            {
                return;
            }

            string resolvedPath = Helper.ResolvePath(StaticContentPath);

            foreach (string filePath in Directory.GetFiles(resolvedPath, "*.*", SearchOption.AllDirectories))
            {
                string resolvedFilePath = filePath.Substring(resolvedPath.Length - 1);

                resolvedFilePath = resolvedFilePath.Replace('\\', '/');

                Reporter.PrintEmphasized(Direction.Action, "Static Content", resolvedFilePath);

                byte[] bytes = File.ReadAllBytes(filePath);

                if (CompressStaticContent)
                {
                    bytes = CompressGzip(bytes);
                }

                StaticContent.Add(resolvedFilePath.ToLowerInvariant(), bytes);
            }
        }

        private async Task StartHandleConnection(TcpClient tcpClient)
        {
            // start the new connection task
            Task connectionTask = EndHandleConnection(tcpClient);

            // catch all errors of HandleConnectionAsync
            try
            {
                await connectionTask;
                // we may be on another thread after "await"
            }
            catch (Exception ex)
            {
                Reporter.PrintException(ex, "Exception while processing REST request");
            }
        }
    }
}