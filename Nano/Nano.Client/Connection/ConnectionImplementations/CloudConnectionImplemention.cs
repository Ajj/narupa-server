﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using System;
using System.Threading;

namespace Nano.Client.ConnectionImplementations
{
    internal class CloudConnectionImplemention : IConnectionImplementation
    {
        private readonly CloudConnectionInfo info;
        private readonly IReporter reporter;
        private readonly TransportClient transportClient;

        public event EventHandler Connected;

        public event EventHandler Disconnected;

        public SimboxConnectionInfo Info => info;

        public ITransportPacketTransmitter Transmitter => transportClient;
        public ConnectionState State => transportClient.State;

        public CloudConnectionImplemention(CloudConnectionInfo info, PacketStatistics packetStatistics, IReporter reporter)
        {
            this.info = info;
            this.reporter = reporter;

            transportClient = new TransportClient(info.ConnectionString, info.SessionKey, packetStatistics, reporter);
            transportClient.Connected += TransportClient_Connected;
            transportClient.Disconnected += TransportClient_Disconnected;
        }

        public void AttachReceiver(ITransportPacketReceiver receiver)
        {
            transportClient.AttachReceiver(receiver);
        }

        public void BeginStart(ClientBase client)
        {
            Thread thread = new Thread(delegate ()
            {
                ManualResetEvent continueEvent = new ManualResetEvent(false);

                void ContinueEventHandler(object sender, EventArgs e)
                {
                    continueEvent.Set();
                }

                transportClient.Connected += ContinueEventHandler;
                transportClient.Disconnected += ContinueEventHandler;

                try
                {
                    transportClient.Start();

                    continueEvent.WaitOne(30000);

                    if (transportClient.State != ConnectionState.Connected &&
                        transportClient.State != ConnectionState.Handshake)
                    {
                        reporter.PrintError("Could not connect to server");
                    }
                    else
                    {
                        //reporter.PrintEmphasized("Connected to server");

                        //Connected?.Invoke(this, EventArgs.Empty);
                        //client.Start();
                    }
                }
                catch (Exception ex)
                {
                    reporter.PrintException(ex, "Exception while creating client.");
                }
                finally
                {
                    transportClient.Connected -= ContinueEventHandler;
                    transportClient.Disconnected -= ContinueEventHandler;
                }
            });

            thread.Start();
        }

        public void Dispose()
        {
            transportClient?.Dispose();
        }

        private void TransportClient_Connected(object sender, EventArgs e)
        {
            reporter.PrintEmphasized("Connected to server");

            Connected?.Invoke(this, EventArgs.Empty);
        }

        private void TransportClient_Disconnected(object sender, EventArgs e)
        {
            Disconnected?.Invoke(sender, e);
        }
    }
}