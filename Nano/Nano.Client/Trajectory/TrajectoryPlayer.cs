﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using System;
using System.IO;
using System.Net;
using System.Threading;

namespace Nano.Client
{
    public class TrajectoryPlayer : IDisposable
    {
        public delegate void TrajectoryPlayerEvent(TrajectoryPlayer player);

        public delegate void TrajectoryPlayerThreadExceptionEvent(TrajectoryPlayer player, Exception ex);

        public readonly IPEndPoint ImposterIPEndPoint;
        private readonly TransportPacketParser parser;
        private readonly ManualResetEvent pauseReadEvent = new ManualResetEvent(false);
        private Thread readThread;
        private bool shouldExit;
        private readonly Stream stream;
        private readonly ManualResetEvent threadExitEvent = new ManualResetEvent(true);

        public event TrajectoryPlayerEvent EndOfStreamReached;

        public event TrajectoryPlayerEvent ReadThreadExited;

        public event TrajectoryPlayerThreadExceptionEvent ThreadException;

        public bool EndOfStream => false;

        public bool Paused { get; private set; }

        public TrajectoryPlayer(IPEndPoint imposterIPEndPoint, ITransportPacketReceiver transportPacketReceiver, PacketStatistics packetStatistics, Stream stream)
        {
            ImposterIPEndPoint = imposterIPEndPoint;

            this.stream = stream;

            parser = new TransportPacketParser(transportPacketReceiver, packetStatistics);

            Start();
        }

        public void ContinueReading()
        {
            Paused = false;
            pauseReadEvent.Set();
        }

        public void Dispose()
        {
            Stop();

            stream.Dispose();
        }

        public void PauseReading()
        {
            Paused = true;
            pauseReadEvent.Reset();
        }

        public void Start()
        {
            Stop();

            shouldExit = false;

            readThread = new Thread(DoRead);
            threadExitEvent.Reset();
            readThread.Start();
        }

        public void Stop()
        {
            shouldExit = true;

            ContinueReading();

            threadExitEvent.WaitOne();
        }

        private void DoRead()
        {
            try
            {
                do
                {
                    pauseReadEvent.WaitOne();

                    if (shouldExit == true)
                    {
                        return;
                    }
                }
                while (parser.ProcessSinglePacket(ImposterIPEndPoint, stream) == true);

                EndOfStreamReached?.Invoke(this);
            }
            catch (Exception ex)
            {
                ThreadException?.Invoke(this, ex);
            }
            finally
            {
                threadExitEvent.Set();
                ReadThreadExited?.Invoke(this);
            }
        }
    }
}