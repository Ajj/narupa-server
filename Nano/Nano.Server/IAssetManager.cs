﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using System.Threading.Tasks;

namespace Nano.Server
{
    public interface IAssetManager : IServerComponent
    {
        Task<Stream> GetAsset(Uri assetUri, IReporter reporter = null);
        
        Task<bool> AssetExists(Uri assetUri, IReporter reporter = null);
    }
}