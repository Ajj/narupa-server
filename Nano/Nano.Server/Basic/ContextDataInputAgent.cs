﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Net;
using Nano.Science.Simulation.Instantiated;
using Nano.Transport.Agents;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using SlimMath;
using TransportContext = Nano.Transport.Comms.TransportContext;

namespace Nano.Server.Basic
{
    public class ContextDataInputAgent : IncomingDataAgent
    {
        public ContextDataInputAgent(TransportContext transportContext, IPEndPoint clientEndpoint)
            : base(transportContext, clientEndpoint)
        {
        }

        protected override void CreateDataStream(TransportDataStreamReceiver receiver,
            ref TransportStreamFormatDescriptor descriptor)
        {
            //@review @mike @ptew Terrible hacks to get interaction structs working.
            switch (receiver.ID)
            {
                case (ushort) StreamID.Interaction:
                    receiver.CreateStream<Interaction>(ref descriptor);
                    break;
                case (ushort) StreamID.VRInteraction:
                case (ushort) StreamID.VRPositions:
                    receiver.CreateStream<VRInteraction>(ref descriptor);
                    break;
                default:
                    switch (descriptor.Type)
                    {
                        case VariableType.Half when descriptor.ChannelCount == 4:
                            receiver.CreateStream<Half4>(ref descriptor);
                            break;
                        case VariableType.Half when descriptor.ChannelCount == 3:
                            receiver.CreateStream<Half3>(ref descriptor);
                            break;
                        case VariableType.Float when descriptor.ChannelCount == 4:
                            receiver.CreateStream<Vector4>(ref descriptor);
                            break;
                        case VariableType.Float when descriptor.ChannelCount == 3:
                            receiver.CreateStream<Vector3>(ref descriptor);
                            break;
                        case VariableType.UInt16 when descriptor.ChannelCount == 2:
                            receiver.CreateStream<BondPair>(ref descriptor);

                            break;
                        case VariableType.Bool when descriptor.ChannelCount == 1:
                            receiver.CreateStream<bool>(ref descriptor);
                            break;
                        case VariableType.Int8 when descriptor.ChannelCount == 1:
                            receiver.CreateStream<sbyte>(ref descriptor);
                            break;
                        case VariableType.UInt8 when descriptor.ChannelCount == 1:
                            receiver.CreateStream<byte>(ref descriptor);
                            break;
                        case VariableType.Int16 when descriptor.ChannelCount == 1:
                            receiver.CreateStream<short>(ref descriptor);
                            break;
                        case VariableType.UInt16 when descriptor.ChannelCount == 1:
                            receiver.CreateStream<ushort>(ref descriptor);
                            break;
                        case VariableType.Int32 when descriptor.ChannelCount == 1:
                            receiver.CreateStream<int>(ref descriptor);
                            break;
                        case VariableType.UInt32 when descriptor.ChannelCount == 1:
                            receiver.CreateStream<uint>(ref descriptor);
                            break;
                        case VariableType.Int64 when descriptor.ChannelCount == 1:
                            receiver.CreateStream<long>(ref descriptor);
                            break;
                        case VariableType.UInt64 when descriptor.ChannelCount == 1:
                            receiver.CreateStream<ulong>(ref descriptor);
                            break;
                        case VariableType.Half when descriptor.ChannelCount == 1:
                            receiver.CreateStream<Half>(ref descriptor);
                            break;
                        case VariableType.Float when descriptor.ChannelCount == 1:
                            receiver.CreateStream<float>(ref descriptor);
                            break;
                        case VariableType.Double when descriptor.ChannelCount == 1:
                            receiver.CreateStream<double>(ref descriptor);
                            break;
                        case VariableType.Vector2:
                            receiver.CreateStream<Vector2>(ref descriptor);
                            break;
                        case VariableType.Vector3:
                            receiver.CreateStream<Vector3>(ref descriptor);
                            break;
                        case VariableType.Byte:
                            receiver.CreateStream<byte>(ref descriptor);
                            break;
                        default:
                            throw new NotImplementedException();
                    }

                    break;
            }
        }
    }
}