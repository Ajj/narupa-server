﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;
using Nano.Science.Simulation.Instantiated;
using Nano.Science.Simulation.Spawning;
using SlimMath;
using System.Collections.Generic;
using System.Xml;

namespace Nano.Science.Simulation.Residues
{
    /// <summary>
    /// Represents an atom for spawning.
    /// </summary>
    [XmlName("Atom")]
    public class AtomMetaData : IAtom
    {
        /// <inheritdoc />
        public Element Element { get; set; }

        /// <inheritdoc />
        public string Name { get; set; }

        /// <inheritdoc />
        public Vector3 Position { get; set; }

        /// <inheritdoc />
        public int AddToTopology(InstantiatedTopology topology, ref SpawnContext context)
        {
            int index = topology.Positions.Count;

            topology.Positions.Add(Vector3.Transform(Position, context.Rotation) + context.Offset);
            topology.Elements.Add(Element);
            string address;
            if (string.IsNullOrEmpty(Name) == false)
            {
                //@review Can we just assume Names are unique within a residue?
                address = AtomPath.Combine(context.Address, Name);
                if (topology.ContainsAddress(address))
                {
                    address = ResolveTypedAddress(topology, context);
                    context.Reporter?.PrintWarning(ReportVerbosity.Normal, $"Cannot uniquely identify the atom with {Name} and index {index}, because another atom with the same name has been added to this atom group. The atom will be given the address {address} to resolve this, but this may cause problems with references.");
                }
            }
            else
            {
                address = AtomPath.Combine(context.Address, context.Index);
            }
            context.Index++;
            topology.AddAddress(address);
            return index;
        }

        /// <summary>
        /// Adds a unique integer after an atom and its name.
        /// </summary>
        /// <param name="topology"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        /// <remarks> While this gets around the requirement for unique atom names within a residue, it introduces lots more problems.</remarks>
        private string ResolveTypedAddress(InstantiatedTopology topology, SpawnContext context)
        {
            string atomTypeAddress = AtomPath.Combine(context.Address, Name);
            // TODO make an indexed lookup for addresses.
            List<string> matchedAtomTypeList = topology.Addresses.FindAll(new System.Predicate<string>(delegate (string s) { return s.StartsWith(atomTypeAddress); }));
            return AtomPath.Combine(atomTypeAddress, matchedAtomTypeList.Count);
        }

        /// <inheritdoc />
        public void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, "Name", null);
            Element = Helper.GetAttributeValue(node, "Element", Element.Virtual);
            Position = Helper.GetAttributeValue(node, "Position", Vector3.Zero);

            if ((int)Element < (int)Element.Virtual || (int)Element >= (int)Element.MAX)
            {
                throw new FlexibleFileFormatException("Atom with invalid element.", node.OuterXml);
            }
        }

        /// <inheritdoc />
        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "Name", Name);
            Helper.AppendAttributeAndValue(element, "Element", Element);
            Helper.AppendAttributeAndValue(element, "Position", Position);

            return element;
        }

    }
}