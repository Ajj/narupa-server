﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Xml;
using Nano.Loading;

namespace Nano.Science.Simulation.Residues
{
    /// <summary>
    /// Internal representation of a link between atoms prior to spawning.
    /// </summary>
    public struct AtomLinkage
    {
        /// <summary>
        /// Default empty atom link.
        /// </summary>
        public static readonly AtomLinkage Empty = default(AtomLinkage);

        /// <summary>
        /// Residue associate with this linkage.
        /// </summary>
        public readonly IResidue Residue;
        /// <summary>
        /// Index of atom associated with linkage.
        /// </summary>
        public readonly int AtomIndex;
        /// <summary>
        /// The atom associated with linkage.
        /// </summary>
        public readonly IAtom Atom;

        /// <summary>
        /// Constructs a linkage from the given residue, atom index and atom.
        /// </summary>
        /// <param name="residue"></param>
        /// <param name="atomIndex"></param>
        /// <param name="atom"></param>
        public AtomLinkage(IResidue residue, int atomIndex, IAtom atom)
        {
            Residue = residue;
            AtomIndex = atomIndex;
            Atom = atom; 
        }     
    }

    /// <summary>
    /// Representation of a bond prior to spawning.
    /// </summary>
    [XmlName("Bond")]
    public class BondMetaData : IBond
    {
        /// <inheritdoc />
        public bool HasLinkage { get; private set; }

        /// <inheritdoc />
        public AtomPath PathToA { get; set; }

        /// <inheritdoc />
        public AtomPath PathToB { get; set; }

        /// <inheritdoc />
        public AtomLinkage A { get; set; }

        /// <inheritdoc />
        public AtomLinkage B { get; set; }

        /// <inheritdoc />
        public virtual bool TryResolveLinkage(IResidue root, bool throwExceptions = false)
        {            
            bool success = true;
            AtomLinkage linkageA = AtomLinkage.Empty, linkageB = AtomLinkage.Empty;

            try
            {
                success &= ResidueUtils.TryResolveLinkage(root, PathToA, out linkageA, throwExceptions);
                success &= ResidueUtils.TryResolveLinkage(root, PathToB, out linkageB, throwExceptions);

                return success;
            }
            catch
            {
                success = false; 

                throw;
            }
            finally
            {
                HasLinkage = success;

                A = linkageA;
                B = linkageB;
            }
        }

        /// <inheritdoc />
        public virtual void Load(LoadContext context, XmlNode node)
        {
            PathToA = new AtomPath(Helper.GetAttributeValue(node, "A", string.Empty));
            PathToB = new AtomPath(Helper.GetAttributeValue(node, "B", string.Empty));
        }

        /// <inheritdoc />
        public virtual XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "A", PathToA.ToString());
            Helper.AppendAttributeAndValue(element, "B", PathToB.ToString());

            return element;
        }

        /// <inheritdoc />
        public virtual bool TryResolvePaths(IResidue root, bool throwExceptions = false)
        {
            if (HasLinkage == false)
            {
                if (throwExceptions == true)
                {
                    throw new FlexibleFileFormatException("Paths cannot be updated as the bond linkage has not been resolved.", string.Empty);
                }
                else
                {
                    return false; 
                }
            }

            bool success = true;

            AtomPath pathA, pathB;

            success &= ResidueUtils.TryResolveBondPath(root, A, out pathA, throwExceptions);
            success &= ResidueUtils.TryResolveBondPath(root, B, out pathB, throwExceptions);

            return success;
        }
    }
}