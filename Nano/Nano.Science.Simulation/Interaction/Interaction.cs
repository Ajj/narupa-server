﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Nano.Transport.Variables.Interaction;
using SlimMath;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents an interaction with the system.
    /// TODO @review This should be refactored out of the API, but forcefield plugins currently need access to it.
    /// </summary>
    public class Interaction : IEquatable<Interaction>
    {
        /// <summary>
        /// Interaction Type.
        /// </summary>
        public readonly InteractionType Type;

        /// <summary>
        /// The position at which the interaction is based.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// PlayerID associated wih interaction.
        /// </summary>
        public ushort PlayerId;

        /// <summary>
        /// Device associated with interaction.
        /// </summary>
        public ushort InputId;

        /// <summary>
        /// The selected atoms.
        /// </summary>
        /// <remarks>
        /// If the <see cref="InteractionType"/> for this interaction is for a single atom, then this collection will only contain the single atom the interaction
        /// applies to, otherwise it will contain all the atoms that this interaction will be applying to.
        /// </remarks>
        public readonly ICollection<int> SelectedAtoms;

        /// <summary>
        /// The centre of mass for the selected atoms.
        /// </summary>
        public Vector3 SelectedAtomsCentreOfMass;

        /// <summary>
        /// Gradient Scale Factor applied to this interaction point.
        /// </summary>
        public readonly float GradientScaleFactor;

        /// <summary>
        /// The total magnitude of force associated with the interaction.
        /// </summary>
        public float ForceMagnitude;

        /// <summary>
        /// The net force of the interaction.
        /// </summary>
        public Vector3 NetForce;

        /// <summary>
        /// The energy of the interaction in kJ/mol.
        /// </summary>
        public float Energy;

        /// <summary>
        /// Initializes a new instance of the <see cref="Interaction"/> struct.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="playerId">Player ID associated with interaction</param>
        /// <param name="inputId">Input ID for the interaction.</param>
        /// <param name="type">Type of interaction.</param>
        /// <param name="gradientScaleFactor">The gradient scale factor multiplier to be used for this interaction.</param>
        /// <param name="selectedAtoms">The set of atoms selected in the interaction.</param>
        public Interaction(Vector3 position, ushort playerId = 0, ushort inputId = 0, InteractionType type = InteractionType.SingleAtom, float gradientScaleFactor = 1f, ICollection<int> selectedAtoms = null)
        {
            Position = position;
            if (selectedAtoms == null)
                SelectedAtoms = new HashSet<int>();
            else
                SelectedAtoms = selectedAtoms;
            GradientScaleFactor = gradientScaleFactor;
            Type = type;
            PlayerId = playerId;
            InputId = inputId;
        }

        /// <summary>
        /// Constructs an instance from the given <see cref="Interaction"/>.
        /// </summary>
        /// <param name="interaction"></param>
        public Interaction(Interaction interaction)
        {
            GradientScaleFactor = interaction.GradientScaleFactor;
            PlayerId = interaction.PlayerId;
            InputId = interaction.InputId;
            Position = interaction.Position;
            SelectedAtoms = interaction.SelectedAtoms;
            Type = interaction.Type;
        }

        /// <inheritdoc />
        public bool Equals(Interaction other)
        {
            if (other == null) return false;
            
            if (Math.Abs(GradientScaleFactor - other.GradientScaleFactor) > 0.001f) return false;
            if (PlayerId != other.PlayerId) return false;
            if (Position != other.Position) return false;
            if (Math.Abs(ForceMagnitude - other.ForceMagnitude) > 0.0001f) return false;
            if (Position != other.Position) return false;
            if (InputId != other.InputId) return false;
            if (!Equals(SelectedAtoms, other.SelectedAtoms)) return false;
            if (Type != other.Type) return false;
            return true;
        }
    }
}