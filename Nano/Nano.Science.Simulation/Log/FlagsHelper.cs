﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Science.Simulation.Log
{
    /// <summary>
    /// Helper class for working with bitmask flags.
    /// </summary>
    /// <remarks>
    /// From: http://stackoverflow.com/questions/3261451/using-a-bitmask-in-c-sharp
    /// </remarks>
    public static class FlagsHelper
    {
        // The casts to object in the below code are an unfortunate necessity due to
        // C#'s restriction against a where T : Enum constraint. (There are ways around
        // this, but they're outside the scope of this simple illustration.)

        /// <summary>
        /// Determine whether a given flag is set to true in a bitmask.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="flags"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public static bool IsSet<T>(T flags, T flag) where T : struct
        {
            int flagsValue = (int)(object)flags;
            int flagValue = (int)(object)flag;

            return (flagsValue & flagValue) != 0;
        }

        /// <summary>
        /// Set a flag in a bitmask.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="flags"></param>
        /// <param name="flag"></param>
        public static void Set<T>(ref T flags, T flag) where T : struct
        {
            int flagsValue = (int)(object)flags;
            int flagValue = (int)(object)flag;

            flags = (T)(object)(flagsValue | flagValue);
        }

        /// <summary>
        /// Unset a flag in a bitmask.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="flags"></param>
        /// <param name="flag"></param>
        public static void Unset<T>(ref T flags, T flag) where T : struct
        {
            int flagsValue = (int)(object)flags;
            int flagValue = (int)(object)flag;

            flags = (T)(object)(flagsValue & (~flagValue));
        }
    }
}