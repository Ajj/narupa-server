﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using QuickGraph;

namespace Nano.Science.Simulation.Instantiated
{
    /// <summary>
    /// A tuple struct representing a bond.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct BondPair : IEquatable<BondPair>, IEdge<ushort>
    {
        /// <summary>
        /// First index in the bond.
        /// </summary>
        public readonly ushort A;

        /// <summary>
        /// Second index in the bond.
        /// </summary>
        public readonly ushort B;

        /// <summary>
        /// Initializes a new instance of the <see cref="BondPair"/> struct.
        /// </summary>
        /// <param name="a"> Index of atom a</param>
        /// <param name="b"> Index of atom b.</param>
        public BondPair(int a, int b) : this()
        {
            if (a < b)
            {
                A = (ushort)a;
                B = (ushort)b;
            }
            else
            {
                A = (ushort)b;
                B = (ushort)a;
            }
        }

        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>The source.</value>
        public ushort Source
        {
            get
            {
                return A;
            }
        }

        /// <summary>
        /// Gets the target.
        /// </summary>
        /// <value>The target.</value>
        public ushort Target
        {
            get
            {
                return B;
            }
        }

        /// <summary>
        /// Determines whether the specified <see cref="BondPair"/> is equal to this instance.
        /// </summary>
        /// <param name="other">The <see cref="BondPair" /> to compare with this instance.</param>
        /// <returns><c>true</c> if the specified <see cref="BondPair" /> is equal to this instance; otherwise, <c>false</c>.</returns>>
        public bool Equals(BondPair other)
        {
            return A == other.A && B == other.B;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            if (obj is BondPair)
            {
                return Equals((BondPair)obj);
            }

            return false;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
        public override int GetHashCode()
        {
            return A << 16 | B;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return $"{A} -> {B}";
        }
    }

    /// <summary>
    /// Represents a list of bonds.
    /// </summary>
    /// <remarks>Stored in SoA form so that streams can be efficiently generated.</remarks>
    public class BondList : IEdgeSet<ushort, Edge<ushort>>, IEquatable<BondList>
    {
        /// <summary> Indexes of bonds. </summary>
        public readonly List<BondPair> BondIndexes = new List<BondPair>();

        /// <summary> Whether the bond can be broken. </summary>
        /// <remarks> Breakable is typically implemented via a Morse potential. </remarks>
        public readonly List<bool> CanBeBroken = new List<bool>();

        /// <summary>
        /// Adds the range of bonds to this instance.
        /// </summary>
        /// <param name="other">The other.</param>
        public void AddRange(BondList other)
        {
            BondIndexes.AddRange(other.BondIndexes);
            CanBeBroken.AddRange(other.CanBeBroken);
        }

        /// <summary> Number of bonds. </summary>
        public int Count { get { return BondIndexes.Count; } }

        /// <summary>
        /// Gets a value indicating whether this instance has any edges.
        /// </summary>
        /// <value><c>true</c> if this instance is edges empty; otherwise, <c>false</c>.</value>
        public bool IsEdgesEmpty
        {
            get
            {
                return BondIndexes.Count == 0;
            }
        }

        /// <summary>
        /// Gets the edge count of this graph.
        /// </summary>
        /// <value>The edge count.</value>
        public int EdgeCount
        {
            get
            {
                return BondIndexes.Count;
            }
        }

        /// <summary>
        /// Gets the edges of this graph.
        /// </summary>
        /// <value>The edges.</value>
        public IEnumerable<Edge<ushort>> Edges
        {
            get
            {
                //TODO this is probably horribly inefficient.
                List<Edge<ushort>> e = new List<Edge<ushort>>();
                foreach (BondPair b in BondIndexes)
                {
                    e.Add(new Edge<ushort>(b.A, b.B));
                }
                return e;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BondList"/> class.
        /// </summary>
        public BondList()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BondList"/> class from the passed instance of a <see cref="BondList"/>.
        /// </summary>
        /// <param name="other">The other <see cref="BondList"/> the instance will copy from.</param>
        public BondList(BondList other)
        {
            AddRange(other);
        }

        /// <summary>
        /// Adds a bond to the list of bonds.
        /// </summary>
        /// <param name="i">The index of first atom in bond.</param>
        /// <param name="j">The index of second atom in bond.</param>
        /// <param name="canBeBroken">if set to <c>true</c> the bond can be broken; otherwise, <c>false</c>.</param>
        public void AddBond(int i, int j, bool canBeBroken = false)
        {
            BondIndexes.Add(new BondPair(i, j));
            CanBeBroken.Add(canBeBroken);
        }

        /// <summary>
        /// Adds the bond to the list of bonds.
        /// </summary>
        /// <param name="bond">The bond.</param>
        /// <param name="canBeBroken">if set to <c>true</c> [can be broken].</param>
        public void AddBond(BondPair bond, bool canBeBroken = false)
        {
            BondIndexes.Add(bond);
            CanBeBroken.Add(canBeBroken);
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            BondIndexes.Clear();
            CanBeBroken.Clear();
        }

        /// <summary>
        /// Removes the specified bond from this instance.
        /// </summary>
        /// <param name="bondPair">The bond pair.</param>
        public void RemoveBond(BondPair bondPair)
        {
            int bondIndex = BondIndexes.IndexOf(bondPair);

            if (bondIndex < 0)
            {
                throw new Exception(string.Format("No bond found for bond pair {0}.", bondPair.ToString()));
            }

            BondIndexes.RemoveAt(bondIndex);

            CanBeBroken.RemoveAt(bondIndex);
        }

        /// <summary>
        /// Gets the atoms specified in the bond list.
        /// </summary>
        /// <returns>HashSet&lt;System.UInt16&gt;.</returns>
        public HashSet<ushort> GetAtoms()
        {
            HashSet<ushort> atoms = new HashSet<ushort>();
            foreach (BondPair b in BondIndexes)
            {
                atoms.Add(b.A);
                atoms.Add(b.B);
            }
            return atoms;
        }

        /// <summary>
        /// /// Determines whether the bond list contains the specified atom.
        /// </summary>
        /// <param name="a">Atom index.</param>
        /// <returns><c>true</c> if the specified a contains atom; otherwise, <c>false</c>.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool ContainsAtom(ushort a)
        {
            foreach (BondPair b in BondIndexes)
            {
                if (b.A == a || b.B == a)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Determines whether this instance contains the specified edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        /// <returns><c>true</c> if this instance contains the specified edge; otherwise, <c>false</c>.</returns>
        public bool ContainsEdge(Edge<ushort> edge)
        {
            return BondIndexes.Contains(new BondPair(edge.Source, edge.Target));
        }

        /// <inheritdoc />
        public bool Equals(BondList other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            foreach (var bond in other.Edges)
            {
                
                if (ContainsEdge(bond) == false)
                    return false;
            }
            foreach (var bond in Edges)
            {
                if (other.ContainsEdge(bond) == false)
                    return false;
            }
            return true;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((BondList) obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                return ((BondIndexes != null ? BondIndexes.GetHashCode() : 0) * 397) ^ (CanBeBroken != null ? CanBeBroken.GetHashCode() : 0);
            }
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>A new copy of this instance.</returns>
        public BondList Clone()
        {
            BondList b = new BondList();
            b.BondIndexes.AddRange(BondIndexes);
            b.CanBeBroken.AddRange(CanBeBroken);
            return b;
        }
    }
}
