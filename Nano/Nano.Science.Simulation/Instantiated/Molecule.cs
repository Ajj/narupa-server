﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using Nano.Science.Simulation.Instantiated;
using QuickGraph;
using SlimMath;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Represents a molecule consisting of a connected set of bonds and atoms.
    /// </summary>
    /// <remarks>
    /// A molecule is defined in the flattened topology, and thus atom indices are absolute.
    /// </remarks>
    public class Molecule
    {
        /// <summary> The indices of nodes that make up this component </summary>
        public readonly HashSet<ushort> AtomIndexes = new HashSet<ushort>();

        /// <summary> Indices of edges that make up this component, aka bonds </summary>
        public readonly BondList Bonds = new BondList();

        /// <summary>
        /// Representation of molecule as undirected graph.
        /// </summary>
        public UndirectedGraph<ushort, Edge<ushort>> Graph = new UndirectedGraph<ushort, Edge<ushort>>();

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>Molecule.</returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Molecule Clone()
        {
            Molecule clone = new Molecule();

            clone.AtomIndexes.UnionWith(AtomIndexes);
            clone.Bonds.AddRange(Bonds);
            clone.GenerateGraph();
            return clone;
        }

        private UndirectedGraph<ushort, Edge<ushort>> GenerateGraph()
        {
            UndirectedGraph<ushort, Edge<ushort>> g = new UndirectedGraph<ushort, Edge<ushort>>(false);
            foreach (ushort a in AtomIndexes)
            {
                g.AddVertex(a);
            }

            g.AddEdgeRange(Bonds.Edges);
            Graph = g;
            return g;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Molecule"/> class.
        /// </summary>
        /// <param name="graph">The graph.</param>
        public Molecule(UndirectedGraph<ushort, Edge<ushort>> graph)
        {
            Graph = graph;
            AtomIndexes.Clear();
            foreach (ushort atom in graph.Vertices)
            {
                AtomIndexes.Add(atom);
            }
            foreach (Edge<ushort> bond in graph.Edges)
            {
                Bonds.AddBond(bond.Source, bond.Target);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Molecule"/> class.
        /// </summary>
        public Molecule()
        {
        }

        /// <summary>
        /// Determines whether [is connected molecule].
        /// </summary>
        /// <returns><c>true</c> if [is connected molecule]; otherwise, <c>false</c>.</returns>
        private bool IsConnectedMolecule()
        {
            if (Bonds.Count == 0 && AtomIndexes.Count == 1)
                return true;
            HashSet<int> edgeNodes = new HashSet<int>();

            foreach (BondPair e in Bonds.BondIndexes)
            {
                edgeNodes.Add(e.A);
                edgeNodes.Add(e.B);
            }

            if (edgeNodes.Count != AtomIndexes.Count)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Adds the bond to this residue.
        /// </summary>
        /// <param name="bond">The bond.</param>
        /// <param name="breakable">if set to <c>true</c> [breakable].</param>
        /// <exception cref="System.Exception">Addition of bond invalid, would result in an unconnected molecule</exception>
        public void AddBond(BondPair bond, bool breakable = false)
        {
            Bonds.AddBond(bond, breakable);
            AtomIndexes.Add(bond.A);
            AtomIndexes.Add(bond.B);
            if (IsConnectedMolecule() == false)
            {
                throw new Exception("Addition of bond invalid, would result in an unconnected molecule");
            }
            Graph.AddEdge(new Edge<ushort>(bond.A, bond.B));
        }

        /// <summary>
        /// Removes the specified bonds from the molecule.
        /// </summary>
        /// <param name="bond">The bond.</param>
        /// <exception cref="System.Exception">Removal of bond splits this molecule into two separate molecules!</exception>
        public void RemoveBondAndAtoms(BondPair bond)
        {
            Bonds.RemoveBond(bond);
            Graph.RemoveEdge(new Edge<ushort>(bond.A, bond.B));
            if (Bonds.ContainsAtom(bond.A) == false)
            {
                AtomIndexes.Remove(bond.A);
                Graph.RemoveVertex(bond.A);
            }
            if (Bonds.ContainsAtom(bond.B) == false)
            {
                AtomIndexes.Remove(bond.B);
                Graph.RemoveVertex(bond.B);
            }

            if (IsConnectedMolecule() == false)
            {
                throw new Exception("Removal of bond splits this molecule into two separate molecules!");
            }
        }

        /// <summary>
        /// Gets the set of first and second neighbours in the molecule.
        /// </summary>
        /// <returns>HashSet&lt;BondPair&gt;.</returns>
        public HashSet<BondPair> GetFirstAndSecondNeighbours()
        {
            HashSet<BondPair> firstAndSecondNeighbours = new HashSet<BondPair>();
            //Loop over each node.
            foreach (ushort node in Graph.Vertices)
            {
                HashSet<ushort> neighbours = new HashSet<ushort>();
                //Get all the neighbours of the node.
                foreach (Edge<ushort> edge in Graph.AdjacentEdges(node))
                {
                    neighbours.Add(edge.Source);
                    neighbours.Add(edge.Target);
                }

                //Add every unique pair of neighbours to the first and second
                //neighbour list.
                var nodeSecondNeighbours = from x in neighbours
                                           from y in neighbours
                                           where x < y
                                           select new BondPair(x, y);
                firstAndSecondNeighbours.UnionWith(nodeSecondNeighbours);
            }
            return firstAndSecondNeighbours;
        }

        /// <summary>
        /// Gets the set of third neighbours in a molecule. Used for calculating 1-4 exceptions.
        /// </summary>
        /// <returns>HashSet&lt;BondPair&gt;.</returns>
        /// <remarks>
        /// TODO Generalise to GetNeighbours(int level).
        /// </remarks>
        public HashSet<BondPair> GetThirdNeighbours()
        {
            HashSet<BondPair> oneTwoThreePairs = GetFirstAndSecondNeighbours();
            HashSet<BondPair> fourNeighbours = new HashSet<BondPair>();

            //Loop over each node.
            foreach (ushort node in Graph.Vertices)
            {
                HashSet<ushort> seenSet = new HashSet<ushort>();
                HashSet<ushort> neighbours = new HashSet<ushort>();

                //Get all the first neighbours of the node.
                foreach (Edge<ushort> edge in Graph.AdjacentEdges(node))
                {
                    neighbours.Add(edge.Source);
                    neighbours.Add(edge.Target);
                }

                seenSet.UnionWith(neighbours);

                //Remove node of interest (don't want to double count)
                neighbours.Remove(node);

                HashSet<ushort> threeNeighbours = new HashSet<ushort>();

                //Get all the neighbours of the two neighbours to get the three neighbours.
                foreach (ushort twoNeighbour in neighbours)
                {
                    foreach (Edge<ushort> edge in Graph.AdjacentEdges(twoNeighbour))
                    {
                        threeNeighbours.Add(edge.Source);
                        threeNeighbours.Add(edge.Target);
                    }

                    threeNeighbours.Remove(twoNeighbour);
                    seenSet.UnionWith(threeNeighbours);
                }

                //Finally, get all the neighbours of the three-neighbours to get the 4-neighbours.
                foreach (ushort threeNeighbour in threeNeighbours)
                {
                    foreach (Edge<ushort> edge in Graph.AdjacentEdges(threeNeighbour))
                    {
                        ushort fourNeighbour = edge.Source == threeNeighbour ? edge.Target : edge.Source;

                        if (seenSet.Contains(fourNeighbour) == false)
                        {
                            fourNeighbours.Add(new BondPair(node, fourNeighbour));
                        }
                    }
                }
            }

            return fourNeighbours;
        }

        /// <summary>
        /// Calculates radius of gyration for this molecule.
        /// </summary>
        /// <param name="particles"></param>
        /// <returns>Radius of Gyration</returns>
        /// <remarks>
        /// Uses the following formula:
        /// 1/N *(sum_i (||r_i - COM||)
        /// </remarks>
        public float GetRadiusOfGyration(ParticleCollection particles)
        {
            //Get COM of molecule.
            Vector3 com = GetCentreOfMass(particles);
            float radiusOfGyration = 0f;
            //Loop over atoms in molecule
            foreach (int atomIndex in AtomIndexes)
            {
                //calculate distance from atom to center of mass
                radiusOfGyration += Vector3.Distance(particles.Position[atomIndex], com);
            }
            //divide through by number of atoms in molecule.
            radiusOfGyration /= AtomIndexes.Count;
            return radiusOfGyration;
        }

        /// <summary>
        /// Gets the centre of mass of the molecule.
        /// </summary>
        /// <param name="particles">The particles.</param>
        /// <returns>Vector3.</returns>
        public Vector3 GetCentreOfMass(ParticleCollection particles)
        {
            Vector3 com = Vector3.Zero;
            float totalMass = 0f;
            foreach (int atomIndex in AtomIndexes)
            {
                com += particles.Mass[atomIndex] * particles.Position[atomIndex];
                totalMass += particles.Mass[atomIndex];
            }
            return com / totalMass;
        }

        /// <summary>
        /// Gets the centre of the molecule.
        /// </summary>
        /// <param name="particles">The particles.</param>
        /// <returns>Vector3.</returns>
        public Vector3 GetCentre(ParticleCollection particles)
        {
            Vector3 center = Vector3.Zero;
            foreach (int atomIndex in AtomIndexes)
            {
                center += particles.Position[atomIndex];
            }
            return center / AtomIndexes.Count;
        }
    }
}