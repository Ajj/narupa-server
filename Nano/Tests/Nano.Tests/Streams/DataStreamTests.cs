﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using NUnit.Framework;

namespace Nano.Tests.Streams
{
    [TestFixture()]
    public class DataStreamTests
    {
        [Test]
        public void DataStream_Test()
        {
            ushort id = 4;
            VariableType type = VariableType.Int32;
            int channels = 1;
            int maxCount = 256;

            int[] initialData = new int[maxCount]; 

            for (int i = 0; i < initialData.Length; i++)
            {
                initialData[i] = i; 
            }

            DataStream<int> stream = new DataStream<int>(id, "Test Stream", new TransportStreamFormatDescriptor(id, type, channels, maxCount));

            initialData.CopyTo(stream.Data, 0);

            stream.Count = maxCount; 

            byte[] bytes = new byte[stream.MaxSizeInBytes + 2 + 4];

            int index = 0;
            stream.CopyTo(bytes, ref index);
       
            Array.Clear(stream.Data, 0, stream.Data.Length);

            index = 0;
            stream.CopyFrom(bytes, ref index);

            for (int i = 0; i < initialData.Length; i++)
            {
                Assert.AreEqual(initialData[i], stream.Data[i]);
            }
        }
    }
}