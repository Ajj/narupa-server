﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using SlimMath;
using System.Runtime.InteropServices;

namespace Nano.Transport.Variables.Interaction
{
    public enum InteractionType
    {
        /// <summary>
        /// No forces to be applied to this interaction
        /// </summary>
        NoInteraction,

        /// <summary>
        /// Apply forces to all atoms within a given cutoff.
        /// </summary>
        AllAtom,

        /// <summary>
        /// Apply forces to a single atom.
        /// </summary>
        SingleAtom,

        /// <summary>
        /// Apply forces to the nearest atom each frame.
        /// </summary>
        NearestAtomUpdate,

        /// <summary>
        /// Apply forces to all the atoms specified in the <see cref="Interaction"/>.
        /// </summary>
        Group
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct Interaction
    {
        /// <summary>
        /// Position of the interaction.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Player ID.
        /// </summary>
        public byte PlayerID;

        /// <summary>
        /// Input ID.
        /// </summary>
        /// <remarks>
        /// In VR, this may be the ID of each controller.
        /// </remarks>
        public byte InputID;

        /// <summary>
        /// Interaction Type;
        /// </summary>
        public byte InteractionType;

        /// <summary>
        /// Gradient Scale Factor.
        /// </summary>
        public float GradientScaleFactor;
    }

    /// <summary>
    /// Struct for transmitting interaction info back to all clients, for visualisation.
    /// </summary>
    /// <remarks>
    /// While the <see cref="Interaction"/> structure contains the information necessary to perform an interaction, this struct contains
    /// the result of applying that interaction to the system, and any additional information that is of use to all clients.
    /// </remarks>
    [StructLayout(LayoutKind.Sequential)]
    public struct InteractionForceInfo
    {
        /// <summary>
        /// The position of the interaction.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// The center of the atoms selected.
        /// </summary>
        public Vector3 SelectedAtomsCentre;

        /// <summary>
        /// The force of the interaction.
        /// </summary>
        public Vector3 Force;

        /// <summary>
        /// The player ID associated with the interaction.
        /// </summary>
        public byte PlayerID;

        /// <summary>
        /// The input ID associated with the interaction.
        /// </summary>
        public byte InputID;
    }
}