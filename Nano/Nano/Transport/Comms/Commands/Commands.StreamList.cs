﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Linq;
using Rug.Osc;

namespace Nano.Transport.Comms
{
    /// <summary>
    /// Client / server communication commands.
    /// </summary>
    public static partial class Commands
    {
        /// <summary>
        /// Holder for all the messages for the stream list commands.
        /// </summary>
        public static class StreamList
        {
            /// <summary>
            /// Create a OSC message for a stream list request command.
            /// </summary>
            /// <returns>A command OSC message.</returns>
            public static OscMessage Request()
            {
                return new OscMessage(GetAddress(Command.ListStreams), PackIdAndState(0, CommandState.Request));
            }

            /// <summary>
            /// Create the OSC messages for a list of streams success response command and the accompanying list items.
            /// </summary>
            /// <param name="listItems">The collection of variables to send.</param>
            /// <returns>An array of OSC message.</returns>
            public static OscMessage[] Success(params ListItem[] listItems)
            {
                List<OscMessage> oscBuilder = new List<OscMessage>
                {
                    new OscMessage(GetAddress(Command.ListStreams), PackIdAndState(0, CommandState.ListHeader), listItems.Length)
                };

                oscBuilder.AddRange(listItems.Select(descriptor => new OscMessage(GetAddress(Command.ListStreams), PackIdAndState(descriptor.ID, CommandState.ListItem), descriptor.Name)));

                return oscBuilder.ToArray();
            }

            /// <summary>
            /// Parse a list stream command OSC message.
            /// </summary>
            /// <param name="message">OSC message to parse.</param>
            /// <param name="id">Outputs the ID of the stream.</param>
            /// <param name="state">Outputs the state of the command.</param>
            /// <param name="error">Outputs the error string of the command if it failed.</param>
            public static void Parse(OscMessage message, out ushort id, out CommandState state, out string error)
            {
                ParseStreamCommand(message, out id, out state, out error);
            }

            /// <summary>
            /// Parse a stream list header from an OSC message.
            /// </summary>
            /// <param name="message">An OSC message.</param>
            /// <param name="id">Outputs the id of the command.</param>
            /// <param name="state">Outputs the command state of the command.</param>
            /// <param name="numberOfItems">Outputs the number of items in the list.</param>
            /// <exception cref="SimboxException">
            /// </exception>
            public static void ParseHeader(OscMessage message, out ushort id, out CommandState state, out int numberOfItems)
            {
                numberOfItems = 0;

                string error;
                Parse(message, out id, out state, out error);

                if (state != CommandState.ListHeader)
                {
                    return;
                }

                if (message.Count < 2)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedNumberOfArguments);
                }

                if ((message[1] is int) == false)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, string.Format(Strings.Error_Argument_WrongType, 1));
                }

                numberOfItems = (int)message[1];
            }

            /// <summary>
            /// Parse a list item from an OSC message.
            /// </summary>
            /// <param name="message">An OSC message.</param>
            /// <param name="id">Outputs the id of the item.</param>
            /// <param name="name">Outputs the name of the item.</param>
            /// <exception cref="SimboxException">
            /// </exception>
            public static void ParseListItem(OscMessage message, out ushort id, out string name)
            {
                CommandState state;
                string error;
                Parse(message, out id, out state, out error);

                if (state == CommandState.Failed)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedCommandState, Strings.Command_UnexpectedCommandState_ListItem);
                }

                if (message.Count < 2)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedNumberOfArguments);
                }

                if ((message[1] is string) == false)
                {
                    throw new SimboxException(SimboxExceptionType.UnexpectedArgumentType, string.Format(Strings.Error_Argument_WrongType, 1));
                }

                name = (string)message[1];
            }
        }
    }
}
