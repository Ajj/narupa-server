﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Comms
{
    public enum ServiceDiscoveryType
    {
        None,
        Ahoy,
        Rest,
    }
}