﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.WebSockets;
using System;
using System.Collections.Generic;

namespace Nano.Transport.Comms.Security
{
    public class ReservationSecurityProvider : ITransportSecurityProvider
    {
        private readonly List<TransportSecuritySessionToken> activeTokens = new List<TransportSecuritySessionToken>();
        private readonly TimeSpan reservationTimeout;
        private readonly TimeSpan? sessionTimeout;
        private readonly object syncLock = new object();
        private string clientKey;
        private DateTime reservationTimestamp;
        private DateTime sessionEnd;

        /// <inheritdoc />
        public event LoadSimulationEvent LoadSimulation;

        public event ReservationExpiredEvent ReservationExpired;

        public event ReservationMadeEvent ReservationMade;

        public event ReservationReleasedEvent ReservationReleased;

        /// <inheritdoc />
        public event ReservedSessionEndedEvent ReservedSessionEnded;

        /// <inheritdoc />
        public int ActiveSessions
        {
            get { lock (syncLock) { return activeTokens.Count; } }
        }

        /// <inheritdoc />
        public TransportSecurityProviderReservationState State { get; private set; }

        public ReservationSecurityProvider(TimeSpan reservationTimeout, TimeSpan? sessionTimeout)
        {
            reservationTimestamp = DateTime.MinValue;

            this.reservationTimeout = reservationTimeout;
            this.sessionTimeout = sessionTimeout;

            sessionEnd = DateTime.MaxValue; 
        }

        /// <inheritdoc />
        public void CheckReservationState()
        {
            lock (syncLock)
            {
                if (State == TransportSecurityProviderReservationState.Unreserved)
                {
                    return; 
                }

                if (DateTime.Now > sessionEnd)
                {
                    sessionEnd = DateTime.MaxValue; 

                    ReservedSessionEnded?.Invoke();
                }

                if (activeTokens.Count > 0)
                {
                    State = TransportSecurityProviderReservationState.SessionActive; 

                    return;
                }

                if (State != TransportSecurityProviderReservationState.SessionActive && 
                    reservationTimestamp >= DateTime.Now - reservationTimeout)
                {
                    return;
                }

                reservationTimestamp = DateTime.MinValue;

                if (State == TransportSecurityProviderReservationState.Reserved)
                {           
                    State = TransportSecurityProviderReservationState.Unreserved;
                    ReservationExpired?.Invoke();
                }
                else
                {                
                    State = TransportSecurityProviderReservationState.Unreserved;
                    ReservationReleased?.Invoke(); 
                }
            }
        }

        /// <inheritdoc />
        public void ReleaseSession(TransportSecuritySessionToken securitySessionToken)
        {
            lock (syncLock)
            {
                if (activeTokens.Count == 0)
                {
                    return;
                }

                activeTokens.Remove(securitySessionToken);

                reservationTimestamp = DateTime.Now;
            }
        }

        /// <inheritdoc />
        public bool TryAquireSession(out TransportSecuritySessionToken securitySessionToken)
        {
            lock (syncLock)
            {
                if (clientKey == null)
                {
                    securitySessionToken = null;

                    return false;
                }

                if (activeTokens.Count == 0 &&
                    reservationTimestamp <= DateTime.Now - reservationTimeout)
                {
                    securitySessionToken = null;

                    return false;
                }

                securitySessionToken = new TransportSecuritySessionToken(this, clientKey);

                activeTokens.Add(securitySessionToken);

                return true;
            }
        }

        /// <inheritdoc />
        public bool TryServerReservation(string simulationPath, out string reservationToken)
        {
            lock (syncLock)
            {
                if (activeTokens.Count > 0)
                {
                    reservationToken = null;

                    return false;
                }

                if (reservationTimestamp >= DateTime.Now - reservationTimeout)
                {
                    reservationToken = null;

                    return false;
                }

                clientKey = Keys.CreateClientKey();

                reservationToken = clientKey;

                reservationTimestamp = DateTime.Now;

                if (sessionTimeout.HasValue == true)
                {
                    sessionEnd = DateTime.Now + sessionTimeout.Value;
                }

                if (string.IsNullOrEmpty(simulationPath) == false)
                {
                    LoadSimulation?.Invoke(simulationPath);
                }

                State = TransportSecurityProviderReservationState.Reserved; 

                ReservationMade?.Invoke();

                return true;
            }
        }
    }
}