﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;

namespace Nano.Loading
{
    /// <summary>
    /// Represents a context used while loading <see cref="ILoadable"/> objects.
    /// </summary>
    public sealed class LoadContext
    {
        /// <summary>
        /// The errors encountered while loading.
        /// </summary>
        public readonly List<string> Errors = new List<string>();

        /// <summary>
        /// The reporter.
        /// </summary>
        public readonly IReporter Reporter;

        /// <summary>
        /// Whether the context has encountered a critical error.
        /// </summary>
        public bool HasHadCriticalError = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadContext"/> class.
        /// </summary>
        /// <param name="reporter">The reporter.</param>
        public LoadContext(IReporter reporter)
        {
            Reporter = reporter;
        }

        /// <summary>
        /// Adds the specified error message to the set of errors encountered during loading.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="isCriticalError">if set to <c>true</c> [is critical error].</param>
        /// <param name="throwException">if set to <c>true</c> [throw exception].</param>
        /// <param name="exception">The exception type to throw.</param>
        public void Error(string message, bool isCriticalError = true, bool throwException = false, SimboxExceptionType exception = SimboxExceptionType.InvalidSimulationInput)
        {
            Errors.Add(message);

            HasHadCriticalError &= isCriticalError;
            if (throwException)
            {
                throw new SimboxException(exception, message);
            }
        }

        /// <summary>
        /// Prints errors to the reporter
        /// </summary>
        /// <returns>System.String.</returns>
        public void ReportErrors()
        {
            foreach (string error in Errors)
            {
                Reporter.PrintError(error);
            }
        }
    }
}