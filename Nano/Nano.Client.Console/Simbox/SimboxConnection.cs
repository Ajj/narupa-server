﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Client.ConnectionImplementations;
using Nano.Transport.Agents;

namespace Nano.Client.Console.Simbox
{
    public class SimboxConnection : SimboxConnectionBase
    {
        public SimboxConnection(
            SimboxConnectionInfo info, 
            IReporter reporter, 
            string recordingPath = null) 
            : base(info, reporter, recordingPath)
        {
        }
        
        public SimboxConnection(
            IConnectionImplementation connectionImplementation, 
            IReporter reporter, 
            string recordingPath = null) 
            : base(connectionImplementation, reporter, recordingPath)
        {
        }

        protected override ClientBase CreateClient(PacketStatistics packetStatistics)
        {
            return new Client(ConnectionImplementation.Transmitter, packetStatistics, Reporter, Info.ConnectionType == SimboxConnectionType.InMemory);
        }
    }
}