// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;

namespace Narupa.Trajectory
{
    /// <summary>
    ///     Represents a typical molecular dynamics trajectory.
    /// </summary>
    /// <remarks>
    ///     In a typical molecular dynamics trajectory, the topology does not change, so each <see cref="Frame" /> has
    ///     the same <see cref="Topology" />.
    /// </remarks>
    public class Trajectory : List<IFrame>, ITrajectory
    {
        /// <summary>
        ///     The initial topology this trajectory was instantiated with.
        /// </summary>
        /// <remarks>
        ///     Note that in a reactive trajectory the topology may change, and thus the trajectory of each <see cref="IFrame" />
        ///     should be used.
        /// </remarks>
        public ITopology Topology => NumberOfFrames > 0 ? this[0].Topology : null;

        /// <inheritdoc />
        public long NumberOfFrames => Count;
        

        /// <summary>
        /// Number of atoms in the trajectory.
        /// </summary>
        public int? NumberOfAtoms => Topology?.NumberOfAtoms;

        /// <inheritdoc />
        public void AddFrame(IFrame frame)
        {
            Add(frame);
        }
    }
}